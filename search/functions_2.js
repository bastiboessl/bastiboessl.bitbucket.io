var searchData=
[
  ['calibration_0',['calibration',['../classTerm__touchpanel_1_1Touchpanel.html#acdf3dce16171290eae03eabd89d47034',1,'Term_touchpanel::Touchpanel']]],
  ['calibration_5fstatus_1',['calibration_status',['../classLab05__BN055_1_1BNO055.html#aa0ef596d41c6813f43d899227fc40759',1,'Lab05_BN055.BNO055.calibration_status()'],['../classTerm__BNO055_1_1BNO055.html#a7e85f2c5f6dddda949237db1e8f67f20',1,'Term_BNO055.BNO055.calibration_status()']]],
  ['change_5foperating_5fmode_2',['change_operating_mode',['../classLab05__BN055_1_1BNO055.html#aa396cab6ff80a824b003815eadd2a450',1,'Lab05_BN055.BNO055.change_operating_mode()'],['../classTerm__BNO055_1_1BNO055.html#a75a3b97cf56e7d8b13f5f6cfa4cc670b',1,'Term_BNO055.BNO055.change_operating_mode()']]],
  ['check_5fuser_5finput_3',['check_user_input',['../classLab03__task__user_1_1Task__User.html#a6eeeb9d3036112915ec51625ae0bdc32',1,'Lab03_task_user.Task_User.check_user_input()'],['../classLab04__task__user_1_1Task__User.html#ab38991891cff218169a4e46b0d5d3188',1,'Lab04_task_user.Task_User.check_user_input()'],['../classTerm__task__user_1_1Task__User.html#a3f2d74814edb564707578b49d00a7942',1,'Term_task_user.Task_User.check_user_input()']]]
];
