/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "A ball balancing platform", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Project Description", "index.html#Project_Descript", null ],
    [ "Mathematical model", "index.html#SystemModel", null ],
    [ "Simulation", "index.html#Simulation1", null ],
    [ "Labs", "index.html#labs", [
      [ "Lab00", "index.html#lab00", null ],
      [ "Lab01", "index.html#sec_lab01", null ],
      [ "Lab02", "index.html#sec_lab02", null ],
      [ "Lab03", "index.html#sec_lab03", null ],
      [ "Lab04", "index.html#sec_lab04", null ],
      [ "Lab05", "index.html#sec_lab05", null ],
      [ "Lab06", "index.html#sec_lab06", null ],
      [ "Term Project", "index.html#terProject", null ]
    ] ],
    [ "Touchpanel Driver Documentation", "touchpanel_doc.html", [
      [ "Hardware Setup", "touchpanel_doc.html#sec_setup", null ],
      [ "Testing Procedure and Results", "touchpanel_doc.html#sec_testing", null ],
      [ "Benchmark", "touchpanel_doc.html#sec_benchmark", null ]
    ] ],
    [ "Term Project", "TermProject.html", [
      [ "Introduction", "TermProject.html#Introduction", [
        [ "Controller", "TermProject.html#Controller", null ],
        [ "Code Structure", "TermProject.html#Design", null ],
        [ "Ball Balancing", "TermProject.html#BallBalancer", null ],
        [ "Functionality of important parts of the program", "TermProject.html#CodeSnippet", null ]
      ] ]
    ] ],
    [ "Source Code", "SourceCode.html", [
      [ "Touchpanel", "SourceCode.html#touchpanel", null ],
      [ "Control Loop", "SourceCode.html#Contrlloop", null ]
    ] ],
    [ "Ball-Balancing", "BallBalancing.html", [
      [ "Balance the platform", "BallBalancing.html#BalancePlatform", null ],
      [ "Balance the ball", "BallBalancing.html#BalanceBall", null ]
    ] ],
    [ "Term-Project diagrams", "Diagrams.html", null ],
    [ "Gain calculation", "gains.html", [
      [ "Open-loop stability", "gains.html#OpenLoopStability", null ],
      [ "Damping ratio and natural frequency", "gains.html#drnf", null ],
      [ "Closed-loop response", "gains.html#closedlooprp", null ]
    ] ],
    [ "Lab Report and Diagrams of Lab04", "lab04_Report.html", [
      [ "Finite State Machines", "lab04_Report.html#sec_diagrams", null ],
      [ "Task Diagram", "lab04_Report.html#sec_tasks", null ],
      [ "Block Diagram", "lab04_Report.html#sec_block", null ],
      [ "Testing Results", "lab04_Report.html#sec_results", null ],
      [ "Plots of the Encoder Data", "lab04_Report.html#sec_plots", null ]
    ] ],
    [ "Project Description", "Project_Description.html", [
      [ "Functionality", "Project_Description.html#function", null ]
    ] ],
    [ "Ball balancer system modeling", "SystemModeling.html", [
      [ "Description", "SystemModeling.html#Description", null ],
      [ "Modeling Assumptions", "SystemModeling.html#ModelingAssumptions", null ],
      [ "Creating the mathematical Model", "SystemModeling.html#Math", [
        [ "Kinematic Relationship", "SystemModeling.html#KinRel", null ],
        [ "Kinematic", "SystemModeling.html#Kinematic", null ],
        [ "Relationship between the motor torque and effective moment applied to the platform", "SystemModeling.html#Relationship", null ],
        [ "Equations of motion", "SystemModeling.html#motion", null ],
        [ "Result", "SystemModeling.html#Result", null ]
      ] ]
    ] ],
    [ "Simulation of the Ball Balancer", "Simulation.html", [
      [ "Preparation for the simulation", "Simulation.html#simulation", null ],
      [ "Matlab Model", "Simulation.html#matlab", [
        [ "Open-loop response", "Simulation.html#openloop", null ],
        [ "Closed-loop response", "Simulation.html#closedloop", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BallBalancing.html",
"classLab04__motordriver_1_1Motor.html#a9ad3b8b142302a31d4eb7eb2740fc8c8"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';