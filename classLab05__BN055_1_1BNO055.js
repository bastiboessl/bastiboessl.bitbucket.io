var classLab05__BN055_1_1BNO055 =
[
    [ "__init__", "classLab05__BN055_1_1BNO055.html#aaaefe585439ae2865f3aedab6e9e0cca", null ],
    [ "calibration_status", "classLab05__BN055_1_1BNO055.html#aa0ef596d41c6813f43d899227fc40759", null ],
    [ "change_operating_mode", "classLab05__BN055_1_1BNO055.html#aa396cab6ff80a824b003815eadd2a450", null ],
    [ "read_angular_velocity", "classLab05__BN055_1_1BNO055.html#a9fc02ee24161a4aecf89a556191c9011", null ],
    [ "read_euler_angles", "classLab05__BN055_1_1BNO055.html#a7559eeb65f8e93f317fbeb11dde2da80", null ],
    [ "ret_calibration_coefficient", "classLab05__BN055_1_1BNO055.html#ad40fc605fc8010b7c94668cd29d759b2", null ],
    [ "wrt_calibration_coefficient", "classLab05__BN055_1_1BNO055.html#aee03a6279c8cc4f26f4d8805e894cfaf", null ]
];