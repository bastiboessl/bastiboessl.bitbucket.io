var classTerm__BNO055_1_1BNO055 =
[
    [ "__init__", "classTerm__BNO055_1_1BNO055.html#aa027a8375c81e64b96f59dd2aa7840ef", null ],
    [ "calibration_status", "classTerm__BNO055_1_1BNO055.html#a7e85f2c5f6dddda949237db1e8f67f20", null ],
    [ "change_operating_mode", "classTerm__BNO055_1_1BNO055.html#a75a3b97cf56e7d8b13f5f6cfa4cc670b", null ],
    [ "read_angular_velocity", "classTerm__BNO055_1_1BNO055.html#a4b8f9af4b763b314df39860d711de674", null ],
    [ "read_euler_angles", "classTerm__BNO055_1_1BNO055.html#a75814fdfc93ce59f7c34c59ab664085f", null ],
    [ "ret_calibration_coefficient", "classTerm__BNO055_1_1BNO055.html#aa6cf3dbab19e452d7114be820b54edb5", null ],
    [ "wrt_calibration_coefficient", "classTerm__BNO055_1_1BNO055.html#ae40b348fe704196592e5c08f66e2b9d5", null ]
];