var annotated_dup =
[
    [ "Lab02_encoder", null, [
      [ "Encoder", "classLab02__encoder_1_1Encoder.html", "classLab02__encoder_1_1Encoder" ]
    ] ],
    [ "Lab02_shares", null, [
      [ "Queue", "classLab02__shares_1_1Queue.html", "classLab02__shares_1_1Queue" ],
      [ "Share", "classLab02__shares_1_1Share.html", "classLab02__shares_1_1Share" ]
    ] ],
    [ "Lab02_task_encoder", null, [
      [ "Task_Encoder", "classLab02__task__encoder_1_1Task__Encoder.html", "classLab02__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab02_task_user", null, [
      [ "Task_User", "classLab02__task__user_1_1Task__User.html", "classLab02__task__user_1_1Task__User" ]
    ] ],
    [ "Lab03_encoder", null, [
      [ "Encoder", "classLab03__encoder_1_1Encoder.html", "classLab03__encoder_1_1Encoder" ]
    ] ],
    [ "Lab03_motordriver", null, [
      [ "DRV8847", "classLab03__motordriver_1_1DRV8847.html", "classLab03__motordriver_1_1DRV8847" ],
      [ "Motor", "classLab03__motordriver_1_1Motor.html", "classLab03__motordriver_1_1Motor" ]
    ] ],
    [ "Lab03_shares", null, [
      [ "Queue", "classLab03__shares_1_1Queue.html", "classLab03__shares_1_1Queue" ],
      [ "Share", "classLab03__shares_1_1Share.html", "classLab03__shares_1_1Share" ]
    ] ],
    [ "Lab03_task_encoder", null, [
      [ "Task_Encoder", "classLab03__task__encoder_1_1Task__Encoder.html", "classLab03__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab03_task_motor", null, [
      [ "Task_Motor", "classLab03__task__motor_1_1Task__Motor.html", "classLab03__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab03_task_user", null, [
      [ "Task_User", "classLab03__task__user_1_1Task__User.html", "classLab03__task__user_1_1Task__User" ]
    ] ],
    [ "Lab04_closedloop", null, [
      [ "ClosedLoop", "classLab04__closedloop_1_1ClosedLoop.html", "classLab04__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab04_encoder", null, [
      [ "Encoder", "classLab04__encoder_1_1Encoder.html", "classLab04__encoder_1_1Encoder" ]
    ] ],
    [ "Lab04_motordriver", null, [
      [ "DRV8847", "classLab04__motordriver_1_1DRV8847.html", "classLab04__motordriver_1_1DRV8847" ],
      [ "Motor", "classLab04__motordriver_1_1Motor.html", "classLab04__motordriver_1_1Motor" ]
    ] ],
    [ "Lab04_shares", null, [
      [ "Queue", "classLab04__shares_1_1Queue.html", "classLab04__shares_1_1Queue" ],
      [ "Share", "classLab04__shares_1_1Share.html", "classLab04__shares_1_1Share" ]
    ] ],
    [ "Lab04_task_encoder", null, [
      [ "Task_Encoder", "classLab04__task__encoder_1_1Task__Encoder.html", "classLab04__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab04_task_motor", null, [
      [ "Task_Motor", "classLab04__task__motor_1_1Task__Motor.html", "classLab04__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab04_task_user", null, [
      [ "Task_User", "classLab04__task__user_1_1Task__User.html", "classLab04__task__user_1_1Task__User" ]
    ] ],
    [ "Lab05_BN055", null, [
      [ "BNO055", "classLab05__BN055_1_1BNO055.html", "classLab05__BN055_1_1BNO055" ]
    ] ],
    [ "Lab06_touchpanel", null, [
      [ "Touchpanel", "classLab06__touchpanel_1_1Touchpanel.html", "classLab06__touchpanel_1_1Touchpanel" ]
    ] ],
    [ "Term_BNO055", null, [
      [ "BNO055", "classTerm__BNO055_1_1BNO055.html", "classTerm__BNO055_1_1BNO055" ]
    ] ],
    [ "Term_closedloop", null, [
      [ "ClosedLoop", "classTerm__closedloop_1_1ClosedLoop.html", "classTerm__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Term_motordriver", null, [
      [ "DRV8847", "classTerm__motordriver_1_1DRV8847.html", "classTerm__motordriver_1_1DRV8847" ],
      [ "Motor", "classTerm__motordriver_1_1Motor.html", "classTerm__motordriver_1_1Motor" ]
    ] ],
    [ "Term_shares", null, [
      [ "Queue", "classTerm__shares_1_1Queue.html", "classTerm__shares_1_1Queue" ],
      [ "Share", "classTerm__shares_1_1Share.html", "classTerm__shares_1_1Share" ]
    ] ],
    [ "Term_task_controller", null, [
      [ "Task_Controller", "classTerm__task__controller_1_1Task__Controller.html", "classTerm__task__controller_1_1Task__Controller" ]
    ] ],
    [ "Term_task_datacollection", null, [
      [ "Task_DataCollection", "classTerm__task__datacollection_1_1Task__DataCollection.html", "classTerm__task__datacollection_1_1Task__DataCollection" ]
    ] ],
    [ "Term_task_imu", null, [
      [ "Task_IMU", "classTerm__task__imu_1_1Task__IMU.html", "classTerm__task__imu_1_1Task__IMU" ]
    ] ],
    [ "Term_task_motor", null, [
      [ "Task_Motor", "classTerm__task__motor_1_1Task__Motor.html", "classTerm__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Term_task_touchpanel", null, [
      [ "Task_Touchpanel", "classTerm__task__touchpanel_1_1Task__Touchpanel.html", "classTerm__task__touchpanel_1_1Task__Touchpanel" ]
    ] ],
    [ "Term_task_user", null, [
      [ "Task_User", "classTerm__task__user_1_1Task__User.html", "classTerm__task__user_1_1Task__User" ]
    ] ],
    [ "Term_touchpanel", null, [
      [ "Touchpanel", "classTerm__touchpanel_1_1Touchpanel.html", "classTerm__touchpanel_1_1Touchpanel" ]
    ] ]
];