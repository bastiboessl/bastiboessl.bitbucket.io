var main_8py =
[
    [ "clear_fault", "main_8py.html#aedaa33e41ff407a376da2c827643b0e7", null ],
    [ "collect_data", "main_8py.html#a4dd4b975c74b85e9ed7fb4f204828e85", null ],
    [ "collect_data_2", "main_8py.html#a4b95b255211c46aff1e0dd76ecdf05c6", null ],
    [ "collect_time", "main_8py.html#a2a50e976b3190289f2ac8eb857308f8f", null ],
    [ "data_list", "main_8py.html#af3a48acf8f56429dd77589c5c9124b2c", null ],
    [ "data_list_2", "main_8py.html#af3ed13f8dbf4472a52a65477e365581d", null ],
    [ "delta", "main_8py.html#a01cf4e8a64081698689afb33f0fc217d", null ],
    [ "delta_2", "main_8py.html#ab5ab83cc7f823420cc97d949c0192d3a", null ],
    [ "duty_m1", "main_8py.html#a7a6118b164d0f6e0c83c16211e31f9d5", null ],
    [ "duty_m2", "main_8py.html#a6dfc6ef2c507f4c7f3cb863a2d427710", null ],
    [ "get_del", "main_8py.html#af3761c343c387015b00527d4c2e3b1f9", null ],
    [ "get_del_2", "main_8py.html#ae800085e0c7f6d0d7661c9dc7ad6e65c", null ],
    [ "get_pos", "main_8py.html#a8e715dd9a7efdeb129ca57f884febcf5", null ],
    [ "get_pos_2", "main_8py.html#a2c618bb5aaae10513c67d0ae4f13cf14", null ],
    [ "get_zero_pos", "main_8py.html#a6a642bfadb38ef6227442c1d846b93c1", null ],
    [ "get_zero_pos_2", "main_8py.html#a9a9071816b19425a9a354a88bfd78c97", null ],
    [ "pos", "main_8py.html#a2b72d44d2090c947f5f8a9bbf595f7b4", null ],
    [ "pos_2", "main_8py.html#ae00561690ffa8ea10d98e5352a4ae15b", null ]
];